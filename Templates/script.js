// const host = document.querySelector("#host");
// const tpl = document.querySelector("template");
// const tplInst = tpl.content.cloneNode(true);  //Clona los hijos y no solo el <template>
//
// host.appendChild(tplInst);

/* =============== Shadow DOM =================*/
class MiSaludo extends HTMLElement{
  constructor(){
    // Obtiene la única etiqueta 'template'
    const tpl = document.querySelector('template');
    // Clono su contenido y se crea una instancia del document fragment
    const tplInst = tpl.content.cloneNode(true);
    super();    // Invoca al constructor de la clase padre
    // Se crea un shadow dom para las intancias de mi-saludo
    this.attachShadow({mode:'open'});
    // Se agrega el template dentro del Shadow DOM usando el elemento raíz 'Shadow Root'
    this.shadowRoot.appendChild(tplInst);
  }
}
// Se registra el custom element para poder ser utilizado declarativamente en el HTML
customElements.define('mi-saludo', MiSaludo);
