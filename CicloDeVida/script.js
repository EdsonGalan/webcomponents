class MiMensaje extends HTMLElement{
  constructor(){
    super();
    this.addEventListener('click', function(e){
      alert("Clic en el mensaje");
    });
    console.log("Constructor: Cuando el elemento es creado");
  }

  static get observedAttributes(){
    return ['msj', 'casi-visible'];
  }

  connectedCallback(){
    console.log("connectedCallback: Cuando el elemento es insertado en el documento");
  }

  disconnectedCallback(){
    alert("disconected: Cuando el elemento es eliminado del documento");
  }

  adoptedCallback(){
    alert("adoptedCallback: Cuando el elemento es adoptado por otro documento");
  }

  attributeChangedCallback(attrName, oldVal, newVal){
    console.log("attributeChangedCallback: Cuando cambia un atributo");
    if(attrName === 'msj'){
      this.pintarMensaje(newVal);
    }
    if(attrName === 'casi-visible'){
      this.setCasiVisible();
    }
  }

  pintarMensaje(msj){
    this.innerHTML = msj;
  }

  get msj(){
    return this.getAttribute('msj');
  }

  set msj(val){
    this.setAttribute('msj',val);
  }

  get casiVisible(){
    return this.hasAttribute('casi-visible');
  }

  set casiVisible(value){
    if(value){
      this.setAttribute('casi-visible','');
    }else{
      this.removeAttribute('casi-bisible');
    }
  }

  // Edita el CSS del elemento basa en la propiedad casi-bisible
  setCasiVisible(){
    if(this.casiVisible){
      this.style.opacity = 0.1;
    }else{
      this.style.opacity = 1;
    }
  }

}

customElements.define('mi-mensaje', MiMensaje);

/* ============================================== */
let miMensaje = document.createElement('mi-mensaje');
miMensaje.msj = "otro Mensaje";
document.body.appendChild(miMensaje);

// También se puede crear con el operador New
let tercerMensaje = new MiMensaje();
tercerMensaje.msj = 'Tercer mensaje';
document.body.appendChild(tercerMensaje);
