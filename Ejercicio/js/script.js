class Table extends HTMLElement{
  constructor(){
    super();
  }
  // Indica que atributos está observando
  static get observedAttributes(){
    return ['search-name', 'reset'];
  }
  // Cuando el elemento es insertado en el documento se dispara esta función
  connectedCallback(){
    this.writeTable();
  }
  // Cuando un atributo es modificado se llama a esta función
  attributeChangedCallback(attrName, oldVal, newVal){
    if(attrName === 'search-name' || attrName === 'reset'){
      // Elimina todos los nodos hijos del componente
      while(this.firstChild){
        this.removeChild(this.firstChild);
      }
      this.writeTable(newVal);
    }
  }
  // Función que pinta toda la información de la API
  writeTable(name=null){
    fetch('http://dummy.restapiexample.com/api/v1/employees')
      .then(response => response.json())
      .then(data => {
        // console.log(data.data);
        /* ..:: Pinta los encabezados ::.. */
        let json = data.data;
        const tabla = document.createElement('table');
        const thead = document.createElement('thead');
        const tr = document.createElement('tr');

        const thid = document.createElement('th');
        const thname = document.createElement('th');
        const thsalary = document.createElement('th');
        const thage = document.createElement('th');
        thid.textContent = "Id";
        thname.textContent = "Name";
        thsalary.textContent = "Salary";
        thage.textContent = "Age";
        tr.appendChild(thid);
        tr.appendChild(thname);
        tr.appendChild(thsalary);
        tr.appendChild(thage);
        thead.appendChild(tr);
        tabla.appendChild(thead);

        const tbody = document.createElement('tbody');

        // Valida si se está recibiendo un dato para hacer la búsqueda
        // Nota: Las líneas de abajo se pueden reducir y evitar repetición
        // pero si ya funciona, ya no le muevas xD
        if( name ){
          for( let item of json ){
            if( item.employee_name.indexOf(name) != -1 ){
              // Si encuentra una coincidencia escribe el registro en la tabla
              const tr_data = document.createElement('tr');
              const tdid = document.createElement('td');
              const tdname = document.createElement('td');
              const tdsalary = document.createElement('td');
              const tdage = document.createElement('td');

              tdid.textContent = item.id;
              tdname.textContent = item.employee_name;
              tdsalary.textContent = item.employee_salary;
              tdage.textContent = item.employee_age;

              tr_data.appendChild(tdid);
              tr_data.appendChild(tdname);
              tr_data.appendChild(tdsalary);
              tr_data.appendChild(tdage);
              tbody.appendChild(tr_data);
            }
          }
          tabla.appendChild(tbody);
          this.appendChild(tabla);
        }else{
          /* ..:: Pinta el cuerpo de la tabla con todos los datos ::.. */
          for( let item of json ){
            const tr_data = document.createElement('tr');
            const tdid = document.createElement('td');
            const tdname = document.createElement('td');
            const tdsalary = document.createElement('td');
            const tdage = document.createElement('td');

            tdid.textContent = item.id;
            tdname.textContent = item.employee_name;
            tdsalary.textContent = item.employee_salary;
            tdage.textContent = item.employee_age;

            tr_data.appendChild(tdid);
            tr_data.appendChild(tdname);
            tr_data.appendChild(tdsalary);
            tr_data.appendChild(tdage);
            tbody.appendChild(tr_data);
          }
          tabla.appendChild(tbody);
          this.appendChild(tabla);
        }
      });
  }
}customElements.define('table-employees', Table);


class BtnSearch extends HTMLElement{
  constructor(){
    super();

    this.addEventListener('click', function(e){
      let nameText = document.getElementById("search").value;
      if( nameText ){
        let component = document.querySelector("table-employees").setAttribute('search-name', nameText);
      }else{
        alert("Type a name...");
      }
    });

    const btn = document.createElement('button');
    btn.setAttribute('class','');
    btn.setAttribute('name','');
    btn.setAttribute('id','');
    btn.textContent = "Search";
    this.appendChild(btn);
  }
}customElements.define('btn-search', BtnSearch);

class BtnClear extends HTMLElement{
  constructor(){
    super();
    this.addEventListener('click', function(e){
      let component = document.querySelector("table-employees").setAttribute('reset','');
      document.querySelector("#search").value = "";
    });
    const btn = document.createElement('button');
    btn.setAttribute('class','');
    btn.setAttribute('name','');
    btn.setAttribute('id','');
    btn.textContent = "Clear";
    this.appendChild(btn);
  }
}customElements.define('btn-clear', BtnClear);
