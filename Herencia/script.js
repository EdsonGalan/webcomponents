class MiBotonExtendido extends HTMLButtonElement{
  constructor(){
    super();
    this.addEventListener('click',(e)=>{
      console.log("Evento click: "+ this.innerHTML);
      alert("MiBotonExtendido");
    });
  }

  static get ceName(){
    return 'mi-boton-extendido';
  }

  get is(){
    return this.getAttribute('is');
  }

  set is(value){
    this.setAttribute('is', value || this.ceName);
  }
}

customElements.define('mi-boton-extendido', MiBotonExtendido,{extends:'button'});

/* ================================================ */
const miBotonExtendido2 = document.createElement('button',{is:MiBotonExtendido.ceName});

miBotonExtendido2.textContent = "Botón Extendido 2";
document.body.appendChild(miBotonExtendido2);

//
const miBotonExtendido3 = document.createElement('button',{is:MiBotonExtendido.ceName});
miBotonExtendido3.textContent = "Botón Extendido 3";
document.querySelector("#container").appendChild(miBotonExtendido3);
